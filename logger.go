package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	printMutex sync.Mutex
	fileMutex  sync.Mutex
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Usage: ./logger <port>")
	}

	port := os.Args[1]

	listener, err := net.Listen("tcp", ":"+port)
	defer listener.Close()

	if err != nil {
		log.Fatalf("Failed to listen on port %s: %v", port, err)
	}

	byteFile, err := os.Create("bytesData.csv")
	defer byteFile.Close()

	delayFile, err := os.Create("delayData.csv")
	defer delayFile.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("Failed to accept connection: %v", err)
			continue
		}
		go handleConnection(conn, byteFile, delayFile)
	}
}

func getTime() float64 {
	currentTime := time.Now()
	nanoseconds := currentTime.UnixNano()
	seconds := float64(nanoseconds) / 1e9
	return seconds
}

func handleConnection(conn net.Conn, byteFile *os.File, delayFile *os.File) {
	defer conn.Close()

	remoteAddr := conn.RemoteAddr().String()

	scanner := bufio.NewScanner(conn)
	var nodeName string
	var currentBytes int
	record := make([]string, 2)

	fileMutex.Lock()
	bytesWriter := csv.NewWriter(byteFile)
	defer bytesWriter.Flush()

	delayWriter := csv.NewWriter(delayFile)
	defer delayWriter.Flush()
	fileMutex.Unlock()

	if scanner.Scan() {
		nodeName = scanner.Text()
		currentTime := getTime()

		fileMutex.Lock()
		currentBytes = len(nodeName)
		record[0] = strconv.FormatFloat(currentTime, 'f', 6, 64)
		record[1] = strconv.Itoa(currentBytes)
		if err := bytesWriter.Write(record); err != nil {
			log.Fatalf("Write error")
		}
		bytesWriter.Flush()
		fileMutex.Unlock()

		safePrintf("%.6f - %s connected\n", currentTime, nodeName)
	} else {
		fmt.Fprintf(os.Stderr, "Error reading from %s: %v\n", remoteAddr, scanner.Err())
	}

	for scanner.Scan() {
		currentBytes = len(scanner.Text())
		currentTime := getTime()

		message := strings.Fields(scanner.Text())

		event := message[1]
		timeReceived := message[0]

		// Saving bytes data
		fileMutex.Lock()
		record[0] = strconv.FormatFloat(currentTime, 'f', 6, 64)
		record[1] = strconv.Itoa(currentBytes)
		if err := bytesWriter.Write(record); err != nil {
			log.Fatalf("Write error")
		}
		bytesWriter.Flush()
		fileMutex.Unlock()

		// Saving Delay Data
		fileMutex.Lock()
		timeReceivedFloat, _ := strconv.ParseFloat(timeReceived, 64)
		currentDelay := currentTime - timeReceivedFloat
		record[0] = strconv.FormatFloat(currentTime, 'f', 6, 64)
		record[1] = strconv.FormatFloat(currentDelay, 'f', 6, 64)
		if err := delayWriter.Write(record); err != nil {
			log.Fatalf("Write error")
		}
		delayWriter.Flush()
		fileMutex.Unlock()

		safePrintf("%s - %s %s\n", timeReceived, nodeName, event)
	}

	safePrintf("%.6f - %s disconnected\n", getTime(), nodeName)
}

func safePrintf(format string, args ...interface{}) {
	printMutex.Lock()
	defer printMutex.Unlock()
	fmt.Printf(format, args...)
}

// ---------------------------------------------------------------------

// package main

// import (
// 	"bufio"
// 	"encoding/csv"
// 	"fmt"
// 	"log"
// 	"net"
// 	"os"
// 	"strconv"
// 	"strings"
// 	"time"
// )

// func main() {
// 	if len(os.Args) < 2 {
// 		log.Fatal("Usage: ./logger <port>")
// 	}

// 	port := os.Args[1]

// 	listener, err := net.Listen("tcp", ":"+port)
// 	defer listener.Close()

// 	if err != nil {
// 		log.Fatalf("Failed to listen on port %s: %v", port, err)
// 	}

// 	byteFile, err := os.Create("bytesData.csv")
// 	defer byteFile.Close()

// 	delayFile, err := os.Create("delayData.csv")
// 	defer delayFile.Close()

// 	for {
// 		conn, err := listener.Accept()
// 		if err != nil {
// 			log.Printf("Failed to accept connection: %v", err)
// 			continue
// 		}
// 		go handleConnection(conn, byteFile, delayFile)
// 	}
// }

// func getTime() float64 {
// 	currentTime := time.Now()
// 	nanoseconds := currentTime.UnixNano()
// 	seconds := float64(nanoseconds) / 1e9
// 	return seconds
// }

// func handleConnection(conn net.Conn, byteFile *os.File, delayFile *os.File) {
// 	defer conn.Close()

// 	remoteAddr := conn.RemoteAddr().String()

// 	scanner := bufio.NewScanner(conn)
// 	var nodeName string
// 	var currentBytes int
// 	record := make([]string, 2)
// 	bytesWriter := csv.NewWriter(byteFile)
// 	defer bytesWriter.Flush()

// 	delayWriter := csv.NewWriter(delayFile)
// 	defer delayWriter.Flush()

// 	if scanner.Scan() {
// 		nodeName = scanner.Text()
// 		currentTime := getTime()

// 		// Saving bytes data
// 		currentBytes = len(nodeName)
// 		record[0] = strconv.FormatFloat(currentTime, 'f', 6, 64)
// 		record[1] = strconv.Itoa(currentBytes)
// 		if err := bytesWriter.Write(record); err != nil {
// 			log.Fatalf("Write error")
// 		}
// 		bytesWriter.Flush()

// 		fmt.Printf("%.6f - %s connected\n", currentTime, nodeName)
// 	} else {
// 		fmt.Fprintf(os.Stderr, "Error reading from %s: %v\n", remoteAddr, scanner.Err())
// 	}

// 	for scanner.Scan() {
// 		currentBytes = len(scanner.Text())
// 		currentTime := getTime()

// 		message := strings.Fields(scanner.Text())

// 		event := message[1]
// 		time := message[0]

// 		// Saving bytes data
// 		record[0] = strconv.FormatFloat(currentTime, 'f', 6, 64)
// 		record[1] = strconv.Itoa(currentBytes)
// 		if err := bytesWriter.Write(record); err != nil {
// 			log.Fatalf("Write error")
// 		}
// 		bytesWriter.Flush()

// 		// Saving Delay Data
// 		timeReceived, _ := strconv.ParseFloat(time, 64)
// 		currentDelay := currentTime - timeReceived
// 		record[0] = strconv.FormatFloat(currentTime, 'f', 6, 64)
// 		record[1] = strconv.FormatFloat(currentDelay, 'f', 6, 64)
// 		if err := delayWriter.Write(record); err != nil {
// 			log.Fatalf("Write error")
// 		}
// 		delayWriter.Flush()

// 		fmt.Printf("%s - %s %s\n", time, nodeName, event)
// 	}

// 	fmt.Fprintf(os.Stderr, "%.6f - %s disconnected\n", getTime(), nodeName)
// }
