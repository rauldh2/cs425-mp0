# CS425-MP0

# Requirements:
- Python
- Go
- Jupyter Notebook
- Install dependencies with `pip install -r requirements.txt`

## Compilation 
- To compile the node run `go build node.go`
- To compile the logger run `go build logger.go`

## Deployment
- To deploy the node run `python3 -u generator.py <rate> | ./node <nodeName> <serverAddress> <port>`
- Node deployment example: `python3 -u generator.py 0.1 | ./node node1 localhost 1234`
- To deploy the logger run `./logger <port>`
- Logger deployment example `./logger 1234`