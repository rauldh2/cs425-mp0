package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"sync"
)

// SafeConn wraps a net.Conn with a mutex to make it thread-safe.
type SafeConn struct {
	conn net.Conn
	mu   sync.Mutex
}

func NewSafeConn(conn net.Conn) *SafeConn {
	return &SafeConn{conn: conn}
}

func (sc *SafeConn) Write(data []byte) (int, error) {
	sc.mu.Lock()
	defer sc.mu.Unlock()
	return sc.conn.Write(data)
}

var printMutex sync.Mutex

func safePrintln(args ...interface{}) {
	printMutex.Lock()
	defer printMutex.Unlock()
	fmt.Println(args...)
}

func main() {
	if len(os.Args) < 4 {
		log.Fatal("Usage: ./node <nodeName> <serverAddress> <port>")
	}

	nodeName := os.Args[1]
	serverAddress := os.Args[2]
	port := os.Args[3]

	conn, err := net.Dial("tcp", serverAddress+":"+port)
	if err != nil {
		log.Fatalf("Failed to connect to server: %v", err)
	}
	defer conn.Close()

	safeConn := NewSafeConn(conn)

	_, err = safeConn.Write([]byte(nodeName + "\n"))
	if err != nil {
		log.Fatalf("Failed to send nodeName: %v", err)
	}

	safePrintln("Connected to server at", serverAddress)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		message := scanner.Text()
		safePrintln("Sending message:", message) // Debug print

		_, err := safeConn.Write([]byte(message + "\n"))
		if err != nil {
			log.Printf("Failed to send message: %v", err)
			break
		}
	}

	if err := scanner.Err(); err != nil {
		log.Printf("Error reading standard input: %v", err)
	}
}
